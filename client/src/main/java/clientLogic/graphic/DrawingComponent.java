package clientLogic.graphic;

import common.Data;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class DrawingComponent extends JComponent {
    private double x0;
    private double y0;
    private java.util.List<Line> lines = new CopyOnWriteArrayList<>();


    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D gr = (Graphics2D) g;
        drawLine(gr);
    }

    private void drawLine(Graphics2D gr) {
        for (Line line : lines) {
            int scaleX0 = (int) (line.x0 * getWidth());
            int scaleY0 = (int) (line.y0 * getHeight());
            int scaleX1 = (int) (line.x1 * getWidth());
            int scaleY1 = (int) (line.y1 * getHeight());
            gr.drawLine(scaleX0, scaleY0, scaleX1, scaleY1);
        }
    }


    public void add(Data data) {
        String command = data.getCommand();
        Line line = new Line(x0, y0, data.getX(), data.getY());
        switch (command) {
            case "move":
                lines.add(line);
            case "start":
                x0 = line.x1;
                y0 = line.y1;
        }
        repaint();
    }
}

