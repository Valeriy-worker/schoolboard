package clientLogic;

import common.ConnectionService;
import common.Data;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class ClientConnection implements ConnectionService {
    private Socket socket;
    private ObjectInputStream input;

    ClientConnection() {
        socket = new Socket();
    }


    @Override
    public int connect(String host) {
        try {
            socket = new Socket(host, DEFAULT_SERVER_PORT);
            input = new ObjectInputStream(socket.getInputStream());
            System.out.println("Connection successful");
        } catch (IOException e) {
            return ERROR;
        }
        return OK;
    }

    @Override
    public Data getData() throws IOException {
        Object obj = null;
        Data data = null;
        try {
            obj = input.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            disconnect();
        }
        if (obj instanceof Data) {
            data = (Data) obj;
        }
        return data;
    }

    @Override
    public void disconnect() {
        if (!socket.isClosed()) {
            try {
                socket.close();
                System.out.println("Socket closed");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
