package serverLogic;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

class ServerSocketThread implements Runnable {

    private Server server;
    private ServerSocket serverSocket;

    ServerSocketThread(Server server) {
        this.server = server;
        serverSocket = server.getServerSocket();
    }

    @Override
    public void run() {
        if (server == null) return;
        while (!serverSocket.isClosed()) {
            try {
                Socket clientSocket = server.getServerSocket().accept();
                Client client = new Client(clientSocket);
                server.addClient(client);
            } catch (IOException e) {
                System.err.println("ServerSocketThread: run(): " + e.getMessage());
            }
        }
    }
}
