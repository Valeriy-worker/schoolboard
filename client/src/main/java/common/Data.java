package common;

import java.io.Serializable;


public class Data implements Serializable {

    private String command;
    private double x;
    private double y;
    private int colour;


    public Data(String command, double x, double y, int colour) {
        this.command = command;
        this.x = x;
        this.y = y;
        this.colour = colour;
    }

    public String getCommand() {
        return command;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getColour() {
        return colour;
    }
}
