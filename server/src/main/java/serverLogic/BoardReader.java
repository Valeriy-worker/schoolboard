package serverLogic;


import common.ConnectionService;
import common.Data;

import java.io.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class BoardReader implements ConnectionService {
    private BufferedReader reader;

    BoardReader(String path) throws FileNotFoundException {
        connect(path);
    }

    @Override
    public int connect(String path) throws FileNotFoundException {
        File file = new File(path);
        reader = new BufferedReader(
                new InputStreamReader(new FileInputStream(file)));
        return OK;
    }

    @Override
    public Data getData() throws IOException {
        String line = reader.readLine();
        if (line == null) {
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return getData();
        }

        Data data = create(line);
        if (data == null) {
            getData();
        }
        return data;
    }

    private Data create(String line) {
        Pattern pattern = Pattern.compile("^(..:){5}..;(move|start);\\-??\\d\\.*\\d+;\\-??\\d\\.*\\d+;\\-??\\d+$");
        Matcher matcher = pattern.matcher(line);
        if (!matcher.matches()) {
            return null;
        }
        String[] arr = line.split(";");
        String command = arr[1];
        double xPoint = Double.valueOf(arr[2]);
        double yPoint = Double.valueOf(arr[3]);
        int colour = Integer.valueOf(arr[4]);
        return new Data(command, xPoint, yPoint, colour);
    }

    @Override
    public void disconnect() {
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
