package serverLogic;

import common.ConnectionService;
import common.Data;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

class Client implements ConnectionService {
    private Socket socket;

    boolean isConnect() {
        return isConnect;
    }

    private boolean isConnect;
    private ObjectOutputStream out;

    Client(Socket socket) {
        this.socket = socket;
        isConnect = connect() == OK;
    }

    void update(Data data) {
        try {

            out.writeObject(data);
        } catch (IOException e) {
            disconnect();
        }
    }

    @Override
    public int connect(String path) {
        return ERROR;
    }

    private int connect() {
        try {
            out = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            System.err.println("Client: connect()");
            e.printStackTrace();
            return ERROR;
        }
        return OK;
    }

    @Override
    public Data getData() {
        return null;
    }

    @Override
    public void disconnect() {
        isConnect = false;
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
