package serverLogic;

import common.ConnectionService;
import common.Data;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

class Server implements ConnectionService {
    private static final String DEFAULT_SOURCE_PATH = "Test.txt";
    private ServerSocket serverSocket;
    private List<Client> clients = new CopyOnWriteArrayList<>();
    private ConnectionService source;
    private int port;

    private Server(int port, String path) throws FileNotFoundException {
        this.port = port;
        source = new BoardReader(path);
    }

    public static void main(String[] args) {
        String path = args.length == 1 ? args[0] : DEFAULT_SOURCE_PATH;
        try {
            Server server = new Server(DEFAULT_SERVER_PORT, path);
            if (server.connect() == OK) {
                server.run();
                server.disconnect();
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
    }

    private void run() {
        if (clients.isEmpty()) {
            System.out.println("no clients");
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            run();
        }
        try {
            while (true) {
                Data data = source.getData();
                updateClients(data);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateClients(Data data) {
        for (Client client : clients) {
            if (client.isConnect()) {
                await();
                client.update(data);
            } else {
                client.disconnect();
                clients.removeIf(c -> c == client);
            }
        }
    }

    private void await() {
        try {
            TimeUnit.MILLISECONDS.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    synchronized void addClient(Client client) {
        clients.add(client);
    }

    @Override
    public int connect(String path) {
        return ERROR;
    }

    private int connect() {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            return ERROR;
        }
        ServerSocketThread serverSocketThread = new ServerSocketThread(this);
        Thread socketThread = new Thread(serverSocketThread);
        socketThread.start();
        return OK;
    }


    ServerSocket getServerSocket() {
        return serverSocket;
    }

    @Override
    public Data getData() {
        return null;
    }

    @Override
    public void disconnect() {
        if (!serverSocket.isClosed()) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                System.err.println("Server: disconnect()");
                e.printStackTrace();
            }
        }
    }
}
