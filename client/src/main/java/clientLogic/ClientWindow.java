package clientLogic;

import common.ConnectionService;
import common.Data;
import clientLogic.graphic.DrawingComponent;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

class ClientWindow extends JFrame {
    private DrawingComponent drawing;
    private ClientConnection connection;
    boolean isClosed;


    private ClientWindow() {
        getGUI();
        WindowListener listener = new WindowListener(this);
        addWindowListener(listener);
        connection = new ClientConnection();
    }

    public static void main(String[] args) {
        ClientWindow window = new ClientWindow();
        String host = args.length == 1 ? args[0] : "localhost";
        if (window.connect(host)){
            window.run();
        }
    }

    private void run() {
        Data data;
        while (true) {
            try {
                data = connection.getData();
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
            if (data == null) {
                continue;
            }
            drawing.add(data);
        }
    }


    private boolean connect(String host) {
        if (isClosed) {
            connection.disconnect();
            return false;
        }
        if (connection.connect(host) == ConnectionService.OK) {
            return true;
        }
        System.out.println("No connection");
        try {
            TimeUnit.SECONDS.sleep(1);
            return connect(host);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
            connection.disconnect();
        }
        return false;
    }

    private void getGUI() {
        setTitle("SchoolBoard");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        Dimension sSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(sSize);
        drawing = new DrawingComponent();
        getContentPane().add(drawing, BorderLayout.CENTER);
        setVisible(true);
    }
}

