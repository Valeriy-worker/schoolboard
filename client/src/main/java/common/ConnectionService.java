package common;

import java.io.IOException;

public interface ConnectionService {

    int OK = 0;

    int ERROR = -1;

    int DISCONNECT = -2;

    int DEFAULT_SERVER_PORT = 29288;

    int connect(String path) throws IOException;

    Data getData() throws IOException;

    void disconnect();
}

