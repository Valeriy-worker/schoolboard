package clientLogic;


import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class WindowListener extends WindowAdapter {
    private ClientWindow window;

    public WindowListener(ClientWindow window) {
        this.window = window;
    }

    @Override
    public void windowClosing(WindowEvent e) {
        window.isClosed=true;
    }
}
