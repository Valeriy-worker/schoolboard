package clientLogic.graphic;

class Line {
    double x0;
    double y0;
    double x1;
    double y1;

    Line(double x0, double y0, double x1, double y1) {
        this.x0 = x0;
        this.y0 = y0;
        this.x1 = x1;
        this.y1 = y1;
    }

}
